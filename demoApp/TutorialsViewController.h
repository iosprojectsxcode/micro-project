//
//  TutorialsViewController.h
//  demoApp
//
//  Created by Johnattan on 28/02/15.
//  Copyright (c) 2015 Johnattan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TutorialsViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIWebView *webView;

@end
