//
//  GameSelectViewController.h
//  demoApp
//
//  Created by Eva Colasso on 27/02/15.
//  Copyright (c) 2015 Johnattan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMSCoinView.h"

@interface GameSelectViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *lblRed;
@property (weak, nonatomic) IBOutlet UILabel *lblGreen;
@property (weak, nonatomic) IBOutlet CMSCoinView *bombilla;
@property (weak, nonatomic) IBOutlet CMSCoinView *servilleta;
@property (weak, nonatomic) IBOutlet CMSCoinView *papel;
@property (weak, nonatomic) IBOutlet CMSCoinView *vidrio;
@property (weak, nonatomic) IBOutlet CMSCoinView *latas;
@property (weak, nonatomic) IBOutlet CMSCoinView *espejo;
@property (weak, nonatomic) IBOutlet CMSCoinView *ceramica;
@property (weak, nonatomic) IBOutlet CMSCoinView *carton;
@property (weak, nonatomic) IBOutlet CMSCoinView *botella;
@property (weak, nonatomic) IBOutlet CMSCoinView *pilas;

@property (weak, nonatomic) IBOutlet UILabel *lblred1;

@property (weak, nonatomic) IBOutlet UILabel *lblred2;

@property (weak, nonatomic) IBOutlet UILabel *lblred3;

@property (weak, nonatomic) IBOutlet UILabel *lblred4;

@property (weak, nonatomic) IBOutlet UILabel *lblred5;

@property (weak, nonatomic) IBOutlet UILabel *lblgreen1;

@property (weak, nonatomic) IBOutlet UILabel *lblgreen2;

@property (weak, nonatomic) IBOutlet UILabel *lblgreen3;

@property (nonatomic, retain) IBOutlet CMSCoinView *coinView;

@property (weak, nonatomic) IBOutlet UILabel *popUp;

@property (weak, nonatomic) IBOutlet UIButton *btnPopUp;

-(void)createItems;

@end
