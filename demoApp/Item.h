//
//  Item.h
//  demoApp
//
//  Created by Johnattan on 27/02/15.
//  Copyright (c) 2015 Johnattan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Item : UIView <UIGestureRecognizerDelegate>

@property float oldX, oldY;

@end
