//
//  Item.m
//  demoApp
//
//  Created by Johnattan on 27/02/15.
//  Copyright (c) 2015 Johnattan. All rights reserved.
//

#import "Item.h"

@implementation Item {
    CGFloat firstX;
    CGFloat firstY;
}

-(void)awakeFromNib {
    UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc]
                                   initWithTarget:self action:@selector(handlePan:)];
    self.gestureRecognizers = @[pan];
    pan.delegate = self;

}

- (void) handlePan: (UIPanGestureRecognizer *) uigr
{
    
    CGPoint translatedPoint = [uigr translationInView:self.superview];
    
    if([uigr state] == UIGestureRecognizerStateBegan) {
        [self.superview bringSubviewToFront:self];
        
        firstX = [self center].x;
        firstY = [self center].y;
    } else if(uigr.state == UIGestureRecognizerStateEnded || uigr.state == UIGestureRecognizerStateCancelled) {
        
        if ([self isInCorrectPosition]) {
            [UIView animateKeyframesWithDuration:0.2 delay:0 options:0 animations:^{
                self.alpha = 0;
                float newX = self.center.x;
                float newY = self.superview.superview.frame.size.height - 40;
                [self setCenter:CGPointMake(newX, newY)];
            } completion:^(BOOL finished) {
                
                //
                if ((int)self.superview.subviews.count == 1) {
                    [[[UIAlertView alloc] initWithTitle:@"¡Bien hecho!" message:@"Ahora sabes como separar los residuos :D" delegate:nil cancelButtonTitle:@"Aceptar" otherButtonTitles:nil, nil] show];
                }
                NSLog(@"Number of children %i", (int)self.superview.subviews.count);
                [self removeFromSuperview];
            }];
        } else {
            [UIView animateWithDuration:0.2f animations:^{
                [self setCenter:CGPointMake(firstX, firstY)];
            }];
        }
        return;
    }
    
    translatedPoint = CGPointMake(firstX + translatedPoint.x, firstY + translatedPoint.y);
    
    [self setCenter:translatedPoint];
}

- (BOOL)isInCorrectPosition {
    CGPoint centerPosition = self.center;
    if (centerPosition.y < self.superview.superview.frame.size.height * 0.5f) return NO;

    return (centerPosition.x > self.superview.frame.size.width / 3 * (self.tag - 1) && centerPosition.x < self.superview.frame.size.width / 3 * self.tag);
}

@end
