//
//  GameSelectViewController.m
//  demoApp
//
//  Created by Eva Colasso on 27/02/15.
//  Copyright (c) 2015 Johnattan. All rights reserved.
//

#import "GameSelectViewController.h"

@interface GameSelectViewController ()

@end

@implementation GameSelectViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSString *smile;
    NSString *sad;
    
    smile = @"\ue802";
    sad =  @"\ue803";
    
    self.lblRed.text = sad;
    self.lblred1.text = sad;
    self.lblred2.text = sad;
    self.lblred3.text = sad;
    self.lblred4.text = sad;
    self.lblred5.text = sad;
    
    self.lblGreen.text = smile;
    self.lblgreen1.text = smile;
    self.lblgreen2.text = smile;
    self.lblgreen3.text = smile;
//    [self.coinView setPrimaryView:self.lblGreen];
//    [self.coinView setSecondaryView:self.lblRed];
    
    [self createItems];
   
    self.popUp.layer.cornerRadius = 5;
    self.popUp.layer.masksToBounds = YES;
    self.btnPopUp.alpha = 0;
    self.popUp.alpha = 0;
    [UIView animateWithDuration:0.6 animations:^{
        self.btnPopUp.alpha = 1;
        self.popUp.alpha = 1;
    }];
    // Do any additional setup after loading the view.
}


-(void)createItems{

//    NSArray *recyclable = @[@"botella", @"papel",@"metal", @"carton", @"lata", @"pilas", @"vidrio"];
//    NSArray *noRecyc = @[@"bombilla", @"ceramica", @"espejo", @"servilleta"];
    
//    UIView *view 
    UIImageView *vidrio = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"vidrio.png"]];
    
    UIImageView *bombilla = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bombilla.jpg"]];
    
    UIImageView *botella = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"botella.jpg"]];
    
    UIImageView *carton = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"carton.jpg"]];
    UIImageView *ceramica = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ceramica.jpg"]];
    
    UIImageView *espejo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"espejo.jpg"]];
    
    UIImageView *lata = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"lata.jpg"]];
    
    UIImageView *papel = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"papel.jpg"]];
    
    UIImageView *pilas = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"pilas.jpg"]];
    
    UIImageView *servilleta = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"servilleta.jpg"]];
    
    [self.vidrio setPrimaryView:vidrio];
    [self.bombilla setPrimaryView:bombilla];
    [self.botella setPrimaryView:botella];
    [self.carton setPrimaryView:carton];
    [self.ceramica setPrimaryView:ceramica];
    [self.espejo setPrimaryView:espejo];
    [self.latas setPrimaryView:lata];
    [self.papel setPrimaryView:papel];
    [self.pilas setPrimaryView:pilas];
    [self.servilleta setPrimaryView:servilleta];
//
    [self.vidrio setSecondaryView:self.lblRed];
    [self.bombilla setSecondaryView:self.lblGreen];
    [self.botella setSecondaryView:self.lblred1];
    [self.carton setSecondaryView:self.lblred2];
    [self.ceramica setSecondaryView:self.lblgreen1];
    [self.espejo setSecondaryView:self.lblgreen2];
    [self.latas setSecondaryView:self.lblred3];
    [self.papel setSecondaryView:self.lblred4];
    [self.pilas setSecondaryView:self.lblred5];
    [self.servilleta setSecondaryView:self.lblgreen3];
//    UIView *viewsTag;
//    int count= 0;
//    while ((viewsTag = [self.view viewWithTag:2])!= nil && count > 3)   {
//            CMSCoinView * view = (CMSCoinView*)viewsTag;
//            [view setSecondaryView:self.lblRed];
//        count++;
//        }
    
    
}

- (IBAction)actionPopUp:(id)sender {
    [UIView animateWithDuration:0.3 animations:^{
        self.btnPopUp.alpha = 0;
        self.popUp.alpha = 0;
    }];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
